# Descripción

Código abierto usando herramientas libres (como `R` o `Python`) para el
procesamiento de microdatos publicados por el Departamento Administrativo
Nacional de Estadística (DANE).
